package com.example.myapplication.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.model.InputString;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;


public class UserDataAdapter extends RecyclerView.Adapter<UserDataAdapter.MyViewHolder> {


    ArrayList<InputString> items = new ArrayList<>(0);
    private Context context;

    public UserDataAdapter( Context context) {
        this.context = context;
    }


    //updating the list
   /* public List<InputString> getItems(){

        return items;
    }

    public void putItem(InputString item){
        items.add(item);
        notifyItemInserted(items.indexOf(item));

    }*/

    public ArrayList<InputString> getItem() {
        return items;
    }

    public void setItems(List<InputString> item) {
        items.clear();
        items.addAll(item);
        notifyDataSetChanged();
    }

    public void addSingleItem(InputString item){
      //  items.clear();
        items.add(0,item);
        notifyDataSetChanged();

    }

    //binding the view of item
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView userData;

        public MyViewHolder(View view) {
            super(view);
            userData = view.findViewById(R.id.userData);

        }
    }

    //inflating the layout
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    //binding the data to the item
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.userData.setText(items.get(position).getValue());


    }

    //getting list count
    @Override
    public int getItemCount() {
        return items.size();
    }

}