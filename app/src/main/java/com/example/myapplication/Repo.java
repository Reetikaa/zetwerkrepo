package com.example.myapplication;

import android.content.Context;

import com.example.myapplication.adapter.UserDataAdapter;
import com.example.myapplication.model.InputString;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;

public class Repo {

    ArrayList<InputString> items = new ArrayList<>(0);


    public List<InputString> getItems(){

        for(String key : Paper.book().getAllKeys()){
            items.add((InputString) Paper.book().read(key));
        }
        return items;
    }

    public void putItem(InputString item){
        Paper.book().write(String.valueOf(getId()), item);


    }

    private long getId(){
        return System.currentTimeMillis();
    }
}
