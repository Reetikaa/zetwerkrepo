package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myapplication.adapter.UserDataAdapter;
import com.example.myapplication.model.InputString;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView recyclerView;
    private UserDataAdapter adapter;
    private ArrayList<InputString> userDataModelList = new ArrayList<>();
    private EditText userInputEdTxt;
    private Button submit;
    private String data;
    Repo repo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Paper.init(this);

        repo = new Repo();

        userInputEdTxt = findViewById(R.id.userText);

        submit = findViewById(R.id.submit);
        submit.setOnClickListener(this);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        adapter = new UserDataAdapter( this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);


        adapter.setItems(repo.getItems());


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.submit:
               updateItem();
                break;
        }
    }

    private void updateItem() {


        data = userInputEdTxt.getText().toString();

        repo.putItem(new InputString(data));


        userInputEdTxt.setText(" ");

       getData();
    }

    private void getData() {

        adapter.addSingleItem(new InputString(data));
        recyclerView.setAdapter(adapter);
    }
}
