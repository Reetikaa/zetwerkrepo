package com.example.myapplication.model;


public class InputString extends StringValueHolder {

    String string = null;


    public InputString(String string){

        this.string = string;

    }

   /* public InputString withString(String string){
        this.string = string;
        return this;
    }*/

    @Override
   public String getValue() {
        return string;
    }
}

abstract class StringValueHolder{

    public abstract String getValue();
}

